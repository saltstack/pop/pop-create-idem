from cloudspec import CloudSpecParam


def test_cloudspecparam_with_nested_cloudspecparam(hub):
    nested_param = CloudSpecParam(
        hub=hub,
        name="nestedParam",
        required=True,
        target="kwargs",
        target_type="mapping",
        member=None,
        param_type="str",
        doc="Some doc",
        default="default-value",
    )
    wrapping_param = CloudSpecParam(
        hub=hub,
        name="wrappingParam",
        required=True,
        target="kwargs",
        target_type="mapping",
        member={"name": "WrappingParamType", "params": {"nestedParam": nested_param}},
        param_type="{}",
        doc="Some doc for the wrapper",
        default=None,
    )
    assert wrapping_param == CloudSpecParam(
        hub=hub,
        name="wrappingParam",
        required=True,
        target="kwargs",
        target_type="mapping",
        member={
            "name": "WrappingParamType",
            "params": {
                "nestedParam": {
                    "required": True,
                    "target": "kwargs",
                    "target_type": "mapping",
                    "member": None,
                    "param_type": "str",
                    "doc": "Some doc",
                    "default": "default-value",
                }
            },
        },
        exclude_from_example=False,
        param_type="{}",
        doc="Some doc for the wrapper",
        default=None,
    )


def test_cloudspecparam_snaked_overrides_default(hub):
    nested_param = CloudSpecParam(
        hub=hub,
        name="param-name",
        required=True,
        target="kwargs",
        target_type="mapping",
        member=None,
        param_type="str",
        doc="Some doc",
        default="default-value",
        snaked="overridden-snaked",
    )
    assert nested_param.snaked == "overridden-snaked"


def test_cloudspecparam_no_snaked(hub):
    nested_param = CloudSpecParam(
        hub=hub,
        name="nestedParam",
        required=True,
        target="kwargs",
        target_type="mapping",
        member=None,
        param_type="str",
        doc="Some doc",
        default="default-value",
    )
    assert nested_param == {
        "required": True,
        "target": "kwargs",
        "target_type": "mapping",
        "member": None,
        "param_type": "str",
        "doc": "Some doc",
        "default": "default-value",
        "snaked": "nested_param",
        "exclude_from_example": False,
    }
