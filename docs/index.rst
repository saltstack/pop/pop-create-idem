.. pop-create-idem documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _README:

.. include:: ../README.rst

.. toctree::
   :hidden:
   :glob:
   :maxdepth: 3
   :caption: Using pop-create-idem

   tutorial/index
   topics/how-to/index
   topics/references/index

.. toctree::
   :caption: Links
   :hidden:

   POP Project Source on GitLab <https://gitlab.com/vmware/pop>
   Idem Project Docs Portal <https://docs.idemproject.io>
   Idem Project Website <https://www.idemproject.io>
   Idem Project Source on GitLab <https://gitlab.com/vmware/idem>
   Project Repository <https://gitlab.com/vmware/idem/pop-create-idem/>
