============================
Customize Auto-Gen Templates
============================

`pop-create-idem` lets users to specify a template directory to customize generated code for exec, state, tools and
tests module which holds jinja2 templates. The user can add any custom business logic inside jinja templates
so that any newly generated modules will have the custom business logic embedded.

.. note::

    If an Idem plugin was generated with pop-create and swagger, there are default templates provided in the
    generated plugin under `{simple_service_name} / autogen / templates` folder.

Pre-Requisites
--------------
* The template directory should follow a specific structure as below:

.. code-block:: bash

    autogen
    └── {simple_service_name}
        └── templates
            ├── exec
            │   ├── create.jinja2
            │   ├── delete.jinja2
            │   ├── get.jinja2
            │   ├── list.jinja2
            │   └── update.jinja2
            ├── state
            │   ├── absent.jinja2
            │   ├── describe.jinja2
            │   └── present.jinja2
            ├── tests
            │   ├── exec
            │   │   ├── test_create.jinja2
            │   │   ├── test_delete.jinja2
            │   │   ├── test_get.jinja2
            │   │   ├── test_list.jinja2
            │   │   └── test_update.jinja2
            │   ├── states
            │   │   ├── test_absent.jinja2
            │   │   ├── test_describe.jinja2
            │   │   └── test_present.jinja2
            │   └── tool
            │       └── default.jinja2
            └── tool
                └── default.jinja2

Example
-------

We will use petstore swagger for generating Idem cloud plugin and customizing jinja template for exec's GET function.

* To generate default templates for Idem plugin generation using swagger, run:

.. code-block:: bash

    pop-create -n idem-petstore --cloud petstore --specification https://petstore.swagger.io/v2/swagger.json --directory /path/to/new/project --create-plugin=templates --author="Petstore Inc."

This should generate default templates that will be used for generating Idem modules.

* Edit `idem-petstore/autogen/petstore/templates/exec/get.jinja2` and change headers input for making API request. Here we are changing to call provider_sdk for GET call and passing api_version header all the time.

.. rstcheck: ignore-next-code-block
.. code-block:: python

    # templates/get.jinja2 #}
    result = dict(comment=[], ret=None, result=True)

    # TODO: Change function methods params if needed
    # CUSTOM SDK request object
    get = await hub.tool.provider_sdk.request(
        ctx,
        method="{{ function.hardcoded.method }}",
        path="{{ function.hardcoded.path }}".format(
            **{{parameter.mapping.path | default({})}}
        ),
        query_params={{parameter.mapping.query | default({})}},
        data={},
        headers={"api_version": "1.0.0"},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # CUSTOM logic
    raw_resource = get["ret"]["content"]

    # TODO: Make sure resource_id is mapped in get response
    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {{function.hardcoded.response_mappings | pprint | indent(12, true)}}
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(parameter_raw)

    result["ret"] = resource_in_present_format
    return result

* Now generate Idem cloud plugin, and run:

.. code-block:: bash

    .. code-block:: bash

    pop-create -n idem-petstore --cloud petstore --specification https://petstore.swagger.io/v2/swagger.json --directory /path/to/new/project --template-dir=/path/to/jinja/templates --author="Petstore Inc."

Newly generated Idem plugin should have new GET function for all resources' exec modules.
